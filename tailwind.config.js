module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: 'var(--primary)',
        'brand-1': {
          darkest: '#1f2d3d',
          dark: '#3c4858',
          DEFAULT: '#c0ccda',
          light: '#e0e6ed',
          lightest: '#f9fafc',
        }
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['children', 'children-first','children-odd', 'children-last']
    },
  },
  plugins: [
    require("./tailwind/form-plugin"),
    require("./tailwind/nth-child"),
    require('tailwindcss-rtl'),
  ],
}
