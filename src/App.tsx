import React from 'react';

function App() {
  React.useEffect(() => {
    document.body.setAttribute('dir', 'ltr');
  }, []);

  const changeDirection = () => {
    const body = document.body;
    const dir = body.getAttribute('dir');
    if (!dir || dir === 'rtl') body.setAttribute('dir', 'ltr');
    else body.setAttribute('dir', 'rtl');
  };
  return (
    <div className="relative h-screen flex flex-col space-y-2 justify-center items-center bg-primary">
      <div className="float-start flex ps-4">
        {/*  dir="rtl" */}
        <button
          className="px-2 rounded bg-gray-900 text-gray-300"
          onClick={changeDirection}
        >
          Direction
        </button>
        <button
          className="dark:text-gray-100 text-gray-800 p-2 rounded"
          onClick={() => {
            document.body.classList.toggle('dark');
          }}
        >
          Toggle
        </button>
      </div>
      <h1>hello world</h1>
      <input type="text" className="form-input" />
      <div className="children:odd:bg-gray-300 flex flex-col space-y-2 w-1/2">
        <div>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus
          explicabo hic harum architecto iure modi omnis possimus fugit! Ullam
          quaerat optio veritatis cumque, recusandae dolore saepe obcaecati
          dicta reiciendis! Nobis!
        </div>
        <div>
          Sunt cupiditate minus facere eveniet in cumque totam quos illo
          facilis, voluptas debitis impedit asperiores esse culpa, hic rem vel
          inventore iusto! Libero eos possimus architecto sapiente impedit,
          expedita numquam!
        </div>
        <div>
          Aperiam adipisci accusantium cumque corporis assumenda tempore dolor,
          rerum autem asperiores porro cum! Odit molestiae quae aliquam
          laudantium dicta exercitationem. Incidunt quo totam mollitia. Hic
          reiciendis accusamus qui quo dolorem?
        </div>
        <div>
          Inventore incidunt nam veniam neque perferendis nihil aliquid tenetur
          sunt cumque aut. Tenetur eos est molestias quod neque culpa aut
          libero! Culpa ratione veniam ducimus odio libero nemo at quae.
        </div>
        <div>
          Ipsa animi blanditiis iusto dicta, vel temporibus doloremque ad magni
          eum quia perferendis mollitia explicabo delectus laborum obcaecati ab
          maxime facere voluptatum, adipisci perspiciatis ex dolore. Tempore
          voluptate ex enim.
        </div>
      </div>
    </div>
  );
}

export default App;
